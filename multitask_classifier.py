import random
import numpy as np
import argparse
from types import SimpleNamespace
from itertools import zip_longest

import torch
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader

from bert import BertModel
from optimizer import AdamW
from tqdm import tqdm

from datasets import SentenceClassificationDataset, SentencePairDataset, \
    load_multitask_data, load_multitask_test_data

from evaluation import model_eval_multitask, test_model_multitask
from pcgrad import PCGrad

import pandas as pd
from transformers import BertTokenizer, get_linear_schedule_with_warmup  
from torch.utils.data import DataLoader, TensorDataset 
import torch.optim as optim 
import string
import spacy # Ivan, update 6
TQDM_DISABLE = True


# fix the random seed


def seed_everything(seed=11711):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True


BERT_HIDDEN_SIZE = 768
N_SENTIMENT_CLASSES = 5


class MultitaskBERT(nn.Module):
    '''
    This module should use BERT for 3 tasks:

    - Sentiment classification (predict_sentiment)
    - Paraphrase detection (predict_paraphrase)
    - Semantic Textual Similarity (predict_similarity)
    '''

    def __init__(self, config):
        super(MultitaskBERT, self).__init__()
        # You will want to add layers here to perform the downstream tasks.
        # Pretrain mode does not require updating bert paramters.
        self.bert = BertModel.from_pretrained('bert-base-uncased', local_files_only=config.local_files_only)
        #num_of_layer = 0
        for param in self.bert.parameters():
            if config.option == 'pretrain':
                param.requires_grad = False
            elif config.option == 'finetune':
                #if(num_of_layer >= 197): # Ivan, update 7
                    #param.requires_grad = True
                param.requires_grad = True
                #else:
                 #   param.requires_grad = False
            #num_of_layer += 1


        self.sst_layer = SSTLayer(config)
        self.paraphrase_layer = ParaphraseLayer(config)
        self.similarity_detection = SimilarityDetectionLayer(config)
        self.general_pretrain_layer = nn.Sequential(  # Ivan, update 5
            nn.Linear(config.hidden_size, 5), 
            nn.ReLU(),
            nn.Linear(5, 5)
        )
        self.lstm_layer = nn.LSTM(config.hidden_size, config.hidden_size // 2, batch_first = True, bidirectional=True) # Ivan, update 1

    def forward(self, input_ids, attention_mask, num_epoch = 11, add_pretrain = False):
        'Takes a batch of sentences and produces embeddings for them.'
        # The final BERT embedding is the hidden state of [CLS] token (the first token)
        # Here, you can start by just returning the embeddings straight from BERT.
        # When thinking of improvements, you can later try modifying this
        # (e.g., by adding other layers).
        # TODO: Improve performance
    
        ### Ivan, update 6 - udditional features based on input_ids
        #num_tokens = torch.sum(attention_mask, dim=1) 
        #tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', local_files_only=args.local_files_only)
        #max_length = 254 
        #input_ids = input_ids[:, :max_length]
        #attention_mask = attention_mask[:, :max_length]
        
        #srl_tokens_list = []
        #nlp = spacy.load("en_core_web_sm")
        #for i in range(input_ids.shape[0]): # SRL embendings as additional feature
         #   input_text = tokenizer.decode(input_ids[i], skip_special_tokens=True)
         #   doc = nlp(input_text)
          #  srl_tokens = [token.dep_ for token in doc]
          #  srl_tokens_list.append(srl_tokens)

        #input_ids_list = []
        #attention_mask_list = []
        #for srl_tokens in srl_tokens_list:
         #   input_text = " ".join(srl_tokens)
          #  input_ids_new = tokenizer.encode(input_text, add_special_tokens=True)
           # if(len(input_ids_new) > max_length):
         #       input_ids_new = input_ids_new[:max_length]
          #  attention_mask_new = [1] * len(input_ids_new)
           # input_ids_list.append(input_ids_new)
         #   attention_mask_list.append(attention_mask_new)

        #padded_input_ids_list = [ids + [0] * (max_length - len(ids)) for ids in input_ids_list]
        #padded_attention_mask_list = [mask + [0] * (max_length - len(mask)) for mask in attention_mask_list]
        
        #input_ids_tensor = torch.tensor(padded_input_ids_list, dtype=torch.long)
        #attention_mask_tensor = torch.tensor(padded_attention_mask_list, dtype=torch.long)
        '''
        special_token_count = torch.sum(input_ids == tokenizer.convert_tokens_to_ids('[SEP]'), dim=1)  # Amount of the special tokens
        capitalized_token_count = torch.sum((input_ids >= 65) & (input_ids <= 90), dim=1)  # Amount of apper tokens
        numeric_token_count = torch.sum(torch.logical_and(input_ids >= 48, input_ids <= 57), dim=1)  # Amount of numeratic tokensa
        sep_token = 251
        for i in range(input_ids.size(0)):
            if input_ids[i, -1] != sep_token and input_ids[i, -1] != 0:
                input_ids[i, -1] = sep_token

        device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        new_features = torch.stack((num_tokens, special_token_count, capitalized_token_count, numeric_token_count), dim=1)
        #input_ids_tensor = input_ids_tensor.to(device)
        #new_features = torch.cat((input_ids_tensor, new_features), dim=1)
        new_features = new_features.to(device)
        input_ids = torch.cat((input_ids, new_features), dim=1)
        #attention_mask_tensor = attention_mask_tensor.to(device)
        #attention_mask = torch.cat((attention_mask, attention_mask_tensor), dim=1)
        attention_mask = torch.cat((attention_mask, torch.ones(attention_mask.size()[0], 4, dtype=torch.int64).to(device)), dim=1)
        '''
        ### Ivan, update 6

        ### Ivan, update 7
        '''
        current_layer = 0
        have_unf = False
        for name, param in self.bert.named_parameters():
            if(param.requires_grad == True):
                have_unf = True
        if(have_unf == True):
            for name, param in self.bert.named_parameters():
                param.requires_grad = False
            for name, param in self.bert.named_parameters():
                if(current_layer > 197 - 17 * (num_epoch + 1)):
                    param.requires_grad = True
                current_layer += 1
        '''
        ### Ivan, update 7

        outputs = self.bert(input_ids=input_ids, attention_mask=attention_mask)
        if(add_pretrain):
            general_pretrain = self.general_pretrain_layer(outputs['pooler_output']) # Ivan, update 5
            return(general_pretrain)
        return(outputs['pooler_output'])
        #output, (last_hidden_state, last_cell_state) = self.lstm_layer(outputs['pooler_output']) # Ivan, update 1
        #return output
        

    def pretrain(self, input_ids, attention_mask):  # Ivan, update 5
        return self.forward(input_ids, attention_mask, True)

    def predict_sentiment(self, input_ids, attention_mask, epoch = 11):
        '''Given a batch of sentences, outputs logits for classifying sentiment.
        There are 5 sentiment classes:
        (0 - negative, 1- somewhat negative, 2- neutral, 3- somewhat positive, 4- positive)
        Thus, your output should contain 5 logits for each sentence.
        '''
        embedding = self.forward(input_ids, attention_mask, epoch)
        return self.sst_layer(embedding)

    def predict_paraphrase(self,
                           input_ids_1, attention_mask_1,
                           input_ids_2, attention_mask_2, epoch = 11, is_training=False):
        '''Given a batch of pairs of sentences, outputs a single logit for predicting whether they are paraphrases.
        Note that your output should be unnormalized (a logit); it will be passed to the sigmoid function
        during evaluation, and handled as a logit by the appropriate loss function.
        '''
        embedding1 = self.forward(input_ids_1, attention_mask_1, epoch)
        embedding2 = self.forward(input_ids_2, attention_mask_2, epoch)

        return self.paraphrase_layer(embedding1, embedding2, is_training)

    def predict_similarity(self,
                           input_ids_1, attention_mask_1,
                           input_ids_2, attention_mask_2, epoch = 11, is_training=False):
        '''Given a batch of pairs of sentences, outputs a single logit corresponding to how similar they are.
        Note that your output should be unnormalized (a logit); it will be passed to the sigmoid function
        during evaluation, and handled as a logit by the appropriate loss function.
        '''
        embedding1 = self.forward(input_ids_1, attention_mask_1, epoch)
        embedding2 = self.forward(input_ids_2, attention_mask_2, epoch)

        return self.similarity_detection(embedding1, embedding2, is_training)


class SSTLayer(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.block1 = MLPBlock(config, layer_sizes=[64, 128])
        self.fc = torch.nn.Linear(128, len(config.num_labels))

    def forward(self, embedding):
        embedding = self.block1(embedding)
        return self.fc(embedding)


class ParaphraseLayer(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.block1 = MLPBlock(config, layer_sizes=[64, 128])
        self.block2 = MLPBlock(config, layer_sizes=[64, 128])
        self.similarity_fn = nn.CosineSimilarity()

    def forward(self, embedding1, embedding2, is_training):
        embedding1 = self.block1(embedding1)
        embedding2 = self.block2(embedding2)

        if is_training:
            return embedding1, embedding2
        
        return self.similarity_fn(embedding1, embedding2)


class SimilarityDetectionLayer(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.cos_similarity_fn = nn.CosineSimilarity()
        self.fc1 = MLPBlock(config, layer_sizes=[64, 128])
        self.fc2 = MLPBlock(config, layer_sizes=[64, 128])

    def forward(self, embedding1, embedding2, is_training):
        embedding1 = self.fc1(embedding1)
        embedding2 = self.fc2(embedding2)

        if is_training:
            return embedding1, embedding2

        return self.cos_similarity_fn(embedding1, embedding2)


class MLPBlock(nn.Module):
    def __init__(self, config, layer_sizes, *args, **kwargs) -> None:
        # Requires minimum 2 layers

        super().__init__(*args, **kwargs)

        self.layers = nn.ModuleList([
            nn.Dropout(0.3),
            nn.Linear(config.hidden_size, layer_sizes[0]),
            nn.ReLU(),
            *[nn.Sequential(nn.Linear(size1, size2), nn.ReLU()) for size1, size2 in zip(layer_sizes, layer_sizes[1:])]
        ])

    def forward(self, embedding):
        for layer in self.layers:
            embedding = layer(embedding)

        return embedding


def save_model(model, optimizer, args, config, filepath):
    save_info = {
        'model': model.state_dict(),
        # 'optim': optimizer.optimizer.state_dict(),
        'optim': optimizer.state_dict(),
        'args': args,
        'model_config': config,
        'system_rng': random.getstate(),
        'numpy_rng': np.random.get_state(),
        'torch_rng': torch.random.get_rng_state(),
    }

    torch.save(save_info, filepath)
    print(f"save the model to {filepath}")


def additional_pretraining_paraphrase(model): # Ivan, update 2
    def tokenize_function(example):
        t1 = []
        t2 = []
        for index, row in tqdm(example.iterrows(), total=len(example)):
            t1.append(row['sentence1'])
            t2.append(row['sentence2'])
        return tokenizer(t1), tokenizer(t2)

    print('ADDITIONAL PRETRAINING PARAPHRASE')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', local_files_only=args.local_files_only)
    tokenized_datasets = pd.read_parquet("Additional_Pretraining_Data/Paws.parquet")
    tokenized_datasets = tokenized_datasets.head(20000)
    tokenized_output1, tokenized_output2 = tokenize_function(tokenized_datasets)

    tokenized_datasets['tokenized_input_ids1'] = tokenized_output1['input_ids']
    tokenized_datasets['tokenized_attention_mask1'] = tokenized_output1['attention_mask']

    tokenized_datasets['tokenized_input_ids2'] = tokenized_output2['input_ids']
    tokenized_datasets['tokenized_attention_mask2'] = tokenized_output2['attention_mask']

    tokenized_input_ids1 = tokenized_datasets['tokenized_input_ids1'].tolist()
    tokenized_attention_mask1 = tokenized_datasets['tokenized_attention_mask1'].tolist()

    tokenized_input_ids2 = tokenized_datasets['tokenized_input_ids2'].tolist()
    tokenized_attention_mask2 = tokenized_datasets['tokenized_attention_mask2'].tolist()
    labels = tokenized_datasets['label'].tolist()
    
    max_seq_length = 512

    tokenized_input_ids1 = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids1]
    tokenized_attention_mask1 = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask1]

    tokenized_input_ids2 = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids2]
    tokenized_attention_mask2 = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask2]

    input_ids1 = torch.tensor(tokenized_input_ids1)
    attention_masks1 = torch.tensor(tokenized_attention_mask1)

    input_ids2 = torch.tensor(tokenized_input_ids2)
    attention_masks2 = torch.tensor(tokenized_attention_mask2)
    labels = torch.tensor(labels)

    dataset = TensorDataset(input_ids1, attention_masks1, input_ids2, attention_masks2, labels)

    batch_size = 16
    train_dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    num_epochs = 2
    total_steps = num_epochs * len(train_dataloader)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    for epoch in range(num_epochs):
        model.train()
        total_loss = 0
        for batch in tqdm(train_dataloader):
            batch = [item.to(device) for item in batch]
            input_ids1, attention_masks1, input_ids2, attention_masks2, labels = batch 

            optimizer.zero_grad()
            logit = model.predict_paraphrase(input_ids1, attention_masks1, input_ids2, attention_masks2, epoch)
            y_hat = logit.sigmoid()
            loss_para = ContrastiveLoss()(y_hat, labels)
            total_loss += loss_para.item()
            loss_para.backward()
            optimizer.step()
            scheduler.step()
        print(epoch, total_loss / len(train_dataloader))

    return model



def additional_pretraining_sst(model): # Ivan, update 3
    def tokenize_function(example):
        t1 = []
        for index, row in tqdm(example.iterrows(), total=len(example)):
            t1.append(row['review'])
        return tokenizer(t1)

    print('ADDITIONAL PRETRAINING SST')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', local_files_only=args.local_files_only)
    tokenized_datasets = pd.read_parquet("Additional_Pretraining_Data/app_reviews.parquet")
    print(tokenized_datasets.shape)
    tokenized_datasets = tokenized_datasets.head(20000)
    tokenized_output = tokenize_function(tokenized_datasets)

    tokenized_datasets['tokenized_input_ids'] = tokenized_output['input_ids']
    tokenized_datasets['tokenized_attention_mask'] = tokenized_output['attention_mask']

    tokenized_input_ids = tokenized_datasets['tokenized_input_ids'].tolist()
    tokenized_attention_mask = tokenized_datasets['tokenized_attention_mask'].tolist()

    labels = tokenized_datasets['star'].tolist()
    for i in range(len(labels)):
        labels[i] -= 1
    
    max_seq_length = 512
    tokenized_input_ids = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids]
    tokenized_attention_mask = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask]
    input_ids = torch.tensor(tokenized_input_ids)
    attention_masks = torch.tensor(tokenized_attention_mask)

    labels = torch.tensor(labels)

    dataset = TensorDataset(input_ids, attention_masks, labels)

    batch_size = 16
    train_dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    num_epochs = 2
    total_steps = num_epochs * len(train_dataloader)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    for epoch in range(num_epochs):
        model.train()
        total_loss = 0
        for batch in tqdm(train_dataloader):
            batch = [item.to(device) for item in batch]
            input_ids, attention_masks, labels = batch 

            optimizer.zero_grad()
            logit = model.predict_sentiment(input_ids, attention_masks, epoch)
            loss_sst = F.cross_entropy(logit, labels.view(-1), reduction='sum')
            total_loss += loss_sst.item()
            loss_sst.backward()
            optimizer.step()
            scheduler.step()
        print(epoch, total_loss / len(train_dataloader))

    return model



def additional_pretraining_sts(model): # Ivan, update 4
    def tokenize_function(example):
        t1 = []
        t2 = []
        for index, row in tqdm(example.iterrows(), total=len(example)):
            t1.append(row['premise'])
            t2.append(row['hypothesis'])
        return tokenizer(t1), tokenizer(t2)

    print('ADDITIONAL PRETRAINING STS')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', local_files_only=args.local_files_only)
    tokenized_datasets = pd.read_parquet("Additional_Pretraining_Data/Dataset_for_sts.parquet")
    print(tokenized_datasets.shape)
    tokenized_output1, tokenized_output2 = tokenize_function(tokenized_datasets)

    tokenized_datasets['tokenized_input_ids1'] = tokenized_output1['input_ids']
    tokenized_datasets['tokenized_attention_mask1'] = tokenized_output1['attention_mask']

    tokenized_datasets['tokenized_input_ids2'] = tokenized_output2['input_ids']
    tokenized_datasets['tokenized_attention_mask2'] = tokenized_output2['attention_mask']

    tokenized_input_ids1 = tokenized_datasets['tokenized_input_ids1'].tolist()
    tokenized_attention_mask1 = tokenized_datasets['tokenized_attention_mask1'].tolist()

    tokenized_input_ids2 = tokenized_datasets['tokenized_input_ids2'].tolist()
    tokenized_attention_mask2 = tokenized_datasets['tokenized_attention_mask2'].tolist()
    labels = tokenized_datasets['relatedness_score'].tolist()
    
    max_seq_length = 512

    tokenized_input_ids1 = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids1]
    tokenized_attention_mask1 = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask1]

    tokenized_input_ids2 = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids2]
    tokenized_attention_mask2 = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask2]

    input_ids1 = torch.tensor(tokenized_input_ids1)
    attention_masks1 = torch.tensor(tokenized_attention_mask1)

    input_ids2 = torch.tensor(tokenized_input_ids2)
    attention_masks2 = torch.tensor(tokenized_attention_mask2)
    labels = torch.tensor(labels)

    dataset = TensorDataset(input_ids1, attention_masks1, input_ids2, attention_masks2, labels)

    batch_size = 16
    train_dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    num_epochs = 3
    total_steps = num_epochs * len(train_dataloader)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    for epoch in range(num_epochs):
        model.train()
        total_loss = 0
        for batch in tqdm(train_dataloader):
            batch = [item.to(device) for item in batch]
            input_ids1, attention_masks1, input_ids2, attention_masks2, labels = batch 

            optimizer.zero_grad()
            logit = model.predict_similarity(input_ids1, attention_masks1, input_ids2, attention_masks2)
            y_hat = logit.sigmoid()
            labels = labels / 5.0
            loss_para = F.mse_loss(y_hat, labels)
            total_loss += loss_para.item()
            loss_para.backward()
            optimizer.step()
            scheduler.step()
        print(epoch, total_loss / len(train_dataloader))

    return model



def additional_pretraining_general(model): # Ivan, update 5
    def remove_punctuation_from_word(word):
        return word.rstrip(string.punctuation)

    def tokenize_function(example):
        t1 = []
        t2 = []
        t1_clean = []
        for index, row in tqdm(example.iterrows(), total=len(example)):
            t1.append(row['text'])
        for text in t1:
            words = text.split()
            if(len(words) > 1):
                last_word = remove_punctuation_from_word(words[-2])
                t2.append(last_word)
                t1_clean.append(' '.join(words[:-2]))
            else:
                t2.append('')
                t1_clean.append('')
        return tokenizer(t1_clean), tokenizer(t2)

    print('ADDITIONAL PRETRAINING GENERAL')
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased', local_files_only=args.local_files_only)
    tokenized_datasets = pd.read_parquet("Additional_Pretraining_Data/text.parquet")
    tokenized_datasets = tokenized_datasets.head(30000)
    tokenized_output, tokenized_output2 = tokenize_function(tokenized_datasets)
    tokenized_datasets['tokenized_input_ids'] = tokenized_output['input_ids']
    tokenized_datasets['tokenized_attention_mask'] = tokenized_output['attention_mask']
    tokenized_datasets['tokenized_input_ids2'] = tokenized_output2['input_ids']
    tokenized_datasets['tokenized_attention_mask2'] = tokenized_output2['attention_mask']

    tokenized_input_ids = tokenized_datasets['tokenized_input_ids'].tolist()
    tokenized_attention_mask = tokenized_datasets['tokenized_attention_mask'].tolist()

    tokenized_input_ids2 = tokenized_datasets['tokenized_input_ids2'].tolist()
    max_seq_length = 512
    tokenized_input_ids = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids]
    tokenized_attention_mask = [mask[:max_seq_length] + [0] * (max_seq_length - len(mask)) if len(mask) < max_seq_length else mask[:max_seq_length] for mask in tokenized_attention_mask]
    max_seq_length = 5
    tokenized_input_ids2 = [ids[:max_seq_length] + [0] * (max_seq_length - len(ids)) if len(ids) < max_seq_length else ids[:max_seq_length] for ids in tokenized_input_ids2]

    input_ids = torch.tensor(tokenized_input_ids)
    attention_masks = torch.tensor(tokenized_attention_mask)
    input_ids2 = torch.tensor(tokenized_input_ids2)

    dataset = TensorDataset(input_ids, attention_masks, input_ids2)

    batch_size = 16
    train_dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    num_epochs = 3
    total_steps = num_epochs * len(train_dataloader)
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0, num_training_steps=total_steps)
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
    for epoch in range(num_epochs):
        model.train()
        total_loss = 0
        for batch in tqdm(train_dataloader):
            batch = [item.to(device) for item in batch]
            input_ids, attention_masks, input_ids2 = batch 
            optimizer.zero_grad()

            outputs = model.pretrain(input_ids, attention_mask=attention_masks)
            cosine_similarities = F.cosine_similarity(outputs, input_ids2, dim=1)
            loss = -torch.mean(cosine_similarities)
            loss.backward()
            total_loss += loss.item()
            optimizer.step()
            scheduler.step()
        print(epoch, total_loss / len(train_dataloader))

    return model



def additional_pretraining(model):
    model = additional_pretraining_general(model) # Ivan, update 5 - additional pretraining (predicting last word). DATASET - https://huggingface.co/datasets/bookcorpus . I want the model to better understang English language
    model = additional_pretraining_paraphrase(model) # Ivan, update 2 - additional pretraining for paraphrase. DATASET - https://huggingface.co/datasets/paws . This corpus contains pairs generated from Wikipedia pages, pairs that are generated from both word swapping and back translation methods.
    model = additional_pretraining_sst(model) # Ivan, update 3 - additional pretraining for sst. DATASET - https://huggingface.co/datasets/app_reviews . This corpus contains app reviews with stars.
    model = additional_pretraining_sts(model) # Ivan, update 4 - additional pretraining for sts. DATASET - https://huggingface.co/datasets/sem_eval_2014_task_1 .
    return(model)


def train_multitask(args):
    device = torch.device('cuda') if args.use_gpu else torch.device('cpu')

    # Create the data and its corresponding datasets and dataloader
    sst_train_data, num_labels, para_train_data, sts_train_data = load_multitask_data(
        args.sst_train, args.para_train, args.sts_train, split='train')
    sst_dev_data, num_labels, para_dev_data, sts_dev_data = load_multitask_data(
        args.sst_dev, args.para_dev, args.sts_dev, split='train')

    # Loading semantic similiarity dataset
    sst_train_data = SentenceClassificationDataset(sst_train_data, args)
    sst_dev_data = SentenceClassificationDataset(sst_dev_data, args)

    sst_train_dataloader = DataLoader(sst_train_data, shuffle=True, batch_size=args.batch_size,
                                      collate_fn=sst_train_data.collate_fn)
    sst_dev_dataloader = DataLoader(sst_dev_data, shuffle=False, batch_size=args.batch_size,
                                    collate_fn=sst_dev_data.collate_fn)

    # Loading paraphrase dataset
    para_train_data = SentencePairDataset(para_train_data, args)
    para_dev_data = SentencePairDataset(para_dev_data, args)

    para_train_dataloader = DataLoader(para_train_data, shuffle=True, batch_size=args.batch_size,
                                       collate_fn=para_train_data.collate_fn)
    para_dev_dataloader = DataLoader(para_dev_data, shuffle=False, batch_size=args.batch_size,
                                     collate_fn=para_dev_data.collate_fn)

    # Loading similarity dataset
    sts_train_data = SentencePairDataset(sts_train_data, args, True)
    sts_dev_data = SentencePairDataset(sts_dev_data, args, True)

    sts_train_dataloader = DataLoader(sts_train_data, shuffle=True, batch_size=args.batch_size,
                                      collate_fn=sts_train_data.collate_fn)

    sts_dev_dataloader = DataLoader(sts_dev_data, shuffle=False, batch_size=args.batch_size,
                                    collate_fn=sts_dev_data.collate_fn)

    # Combining all the dataloaders for a joint training
    all_train_dataloaders = [sst_train_dataloader, para_train_dataloader, sts_train_dataloader]

    # Init model
    config = {'hidden_dropout_prob': args.hidden_dropout_prob,
              'num_labels': num_labels,
              'hidden_size': 768,
              'data_dir': '.',
              'option': args.option,
              'local_files_only': args.local_files_only}

    config = SimpleNamespace(**config)

    model = MultitaskBERT(config)
    model = model.to(device)
    print(device)

    ####### Ivan, update 2, 3, 4, 5 - additional pretrainings
    #model = additional_pretraining(model)

    lr = args.lr
    optimizer = AdamW(model.parameters(), lr=lr)
    # Causes memory overflow but worths investigating
    # If using, uncomment lines in training and save_model
    # optimizer = PCGrad(optimizer)
    best_score = 0

    # Computing weights for weighted losses
    inverse_weights = [1 / len(d) for d in all_train_dataloaders]
    sum_inverse_weight = sum(inverse_weights)
    sst_weight, para_weight, sts_weight = [weight / sum_inverse_weight for weight in inverse_weights]

    print(sst_weight, para_weight, sts_weight)

    for epoch in range(args.epochs):
        model.train()
        print('TRAINING')
        train_loss = train_multitask_jointly(all_train_dataloaders, [sst_weight, para_weight, sts_weight],
                                             model, optimizer, epoch, device)
        # train_loss = train_multitask_separately(sst_train_dataloader, para_train_dataloader, sts_train_dataloader,
        #                                         sst_weight, para_weight, sts_weight,
        #                                         model, optimizer, epoch, device)

        print('VALIDATING')
        # Ilia: If we need the info, we can just compute it during training to spare the resources, 
        # don't think there will be any significant difference
        # TODO: Integrate into train
        paraphrase_accuracy_tr, _, _, sentiment_accuracy_tr, _, _, sts_corr_tr, _, _ = model_eval_multitask(
            sst_train_dataloader, para_train_dataloader, sts_train_dataloader, model, device)
        paraphrase_accuracy_dev, _, _, sentiment_accuracy_dev, _, _, sts_corr_dev, _, _ = model_eval_multitask(
            sst_dev_dataloader, para_dev_dataloader, sts_dev_dataloader, model, device)

        score_sum = paraphrase_accuracy_dev + sentiment_accuracy_dev + sts_corr_dev

        if score_sum > best_score:
            best_score = score_sum
            save_model(model, optimizer, args, config, args.filepath)

        print(
            f"Epoch {epoch}: train loss :: {train_loss :.3f}, train scores (para, sst, sts) :: {paraphrase_accuracy_tr :.3f}, {sentiment_accuracy_tr :.3f}, {sts_corr_tr :.3f}, dev scores (para, sst, sts):: {paraphrase_accuracy_dev :.3f}, {sentiment_accuracy_dev :.3f}, {sts_corr_dev :.3f}")


class ContrastiveLoss(nn.Module):
    def __init__(self, margin=0.2):
        super(ContrastiveLoss, self).__init__()
        self.margin = margin

    def forward(self, predicted_similarity, target_similarity):
        loss = 0.5 * (1 - target_similarity) * predicted_similarity ** 2 + \
               0.5 * (1 + target_similarity) * torch.relu(self.margin - predicted_similarity) ** 2

        return loss.mean()


def train_multitask_jointly(train_dataloaders, weights,
                            model, optimizer, epoch, device):
    num_batches = 0
    train_loss = 0

    # train_dataloaders = [train_dataloaders[0], train_dataloaders[2]]

    for batch_sst, batch_para, batch_sts in tqdm(zip_longest(*train_dataloaders), desc=f'train-{epoch}'):
        if batch_sst is not None:
            sst_ids, sst_mask, sst_labels = (batch_sst['token_ids'],
                                             batch_sst['attention_mask'], batch_sst['labels'])
            sst_ids = sst_ids.to(device)
            sst_mask = sst_mask.to(device)
            sst_labels = sst_labels.to(device)

        if batch_para is not None:
            (para_ids1, para_mask1,
             para_ids2, para_mask2,
             para_labels) = (batch_para['token_ids_1'], batch_para['attention_mask_1'],
                             batch_para['token_ids_2'], batch_para['attention_mask_2'],
                             batch_para['labels'])
            para_ids1 = para_ids1.to(device)
            para_mask1 = para_mask1.to(device)
            para_ids2 = para_ids2.to(device)
            para_mask2 = para_mask2.to(device)
            para_labels = para_labels.to(device).float()

        if batch_sts is not None:
            (sts_ids1, sts_mask1,
             sts_ids2, sts_mask2,
             sts_labels) = (batch_sts['token_ids_1'], batch_sts['attention_mask_1'],
                            batch_sts['token_ids_2'], batch_sts['attention_mask_2'],
                            batch_sts['labels'])
            sts_ids1 = sts_ids1.to(device)
            sts_mask1 = sts_mask1.to(device)
            sts_ids2 = sts_ids2.to(device)
            sts_mask2 = sts_mask2.to(device)
            sts_labels = sts_labels.to(device).float()

        if batch_sst is not None:
            logits = model.predict_sentiment(sst_ids, sst_mask, epoch)
            loss_sst = F.cross_entropy(logits, sst_labels.view(-1), reduction='sum')
        else:
            loss_sst = 0

        if batch_para is not None:
            # emb1, emb2 = model.predict_paraphrase(para_ids1, para_mask1, para_ids2, para_mask2, is_training=True)
            # loss_para = F.cosine_embedding_loss(emb1, emb2, para_labels)
            logit = model.predict_paraphrase(para_ids1, para_mask1, para_ids2, para_mask2, epoch)
            y_hat = logit.sigmoid()
            loss_para = ContrastiveLoss()(y_hat, para_labels)
        else:
            loss_para = 0

        if batch_sts is not None:
            # emb1, emb2 = model.predict_similarity(sts_ids1, sts_mask1, sts_ids2, sts_mask2, is_training=True)
            # loss_sts = F.cosine_embedding_loss(emb1, emb2, sts_labels)
            logit = model.predict_similarity(sts_ids1, sts_mask1, sts_ids2, sts_mask2, epoch)
            y_hat = logit.sigmoid()
            # scaling output to take care of the sigmoid fxn
            sts_labels = sts_labels / 5.0
            loss_sts = F.mse_loss(y_hat, sts_labels)
        else:
            loss_sts = 0

        losses = [loss_sst, loss_para, loss_sts]
        losses = [weight * loss for weight, loss in zip(weights, losses)]
        loss = sum(losses)

        loss.backward()
        # Used for PCGrad
        # optimizer.pc_backward(losses)
        optimizer.step()
        optimizer.zero_grad()

        train_loss += loss.item()
        num_batches += 1

    return train_loss / num_batches


def train_multitask_separately(sst_train_dataloader, para_train_dataloader, sts_train_dataloader,
                               sst_weight, para_weight, sts_weight,
                               model, optimizer, epoch, device):
    num_batches = 0
    train_loss = 0

    print('Training on SST')
    for batch in tqdm(sst_train_dataloader, desc=f'train-sst-{epoch}'):
        b_ids, b_mask, b_labels = (batch['token_ids'],
                                   batch['attention_mask'], batch['labels'])

        b_ids = b_ids.to(device)
        b_mask = b_mask.to(device)
        b_labels = b_labels.to(device)

        optimizer.zero_grad()
        logits = model.predict_sentiment(b_ids, b_mask, epoch)
        loss = sst_weight * F.cross_entropy(logits, b_labels.view(-1), reduction='sum')

        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        num_batches += 1

    print('Training on Paraphrase')
    for batch in tqdm(para_train_dataloader, desc=f'train-paraphrase-{epoch}'):
        (b_ids1, b_mask1,
         b_ids2, b_mask2,
         b_labels) = (batch['token_ids_1'], batch['attention_mask_1'],
                      batch['token_ids_2'], batch['attention_mask_2'],
                      batch['labels'])

        b_ids1 = b_ids1.to(device)
        b_mask1 = b_mask1.to(device)
        b_ids2 = b_ids2.to(device)
        b_mask2 = b_mask2.to(device)
        b_labels = b_labels.to(device).float()

        logit = model.predict_paraphrase(b_ids1, b_mask1, b_ids2, b_mask2, epoch)
        y_hat = logit.sigmoid()
        loss = para_weight * F.binary_cross_entropy(y_hat, b_labels)

        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        num_batches += 1

    print('Training on Similarity')
    for batch in tqdm(sts_train_dataloader, desc=f'train-sts-{epoch}'):
        (b_ids1, b_mask1,
         b_ids2, b_mask2,
         b_labels) = (batch['token_ids_1'], batch['attention_mask_1'],
                      batch['token_ids_2'], batch['attention_mask_2'],
                      batch['labels'])

        b_ids1 = b_ids1.to(device)
        b_mask1 = b_mask1.to(device)
        b_ids2 = b_ids2.to(device)
        b_mask2 = b_mask2.to(device)
        b_labels = b_labels.to(device).float()

        logit = model.predict_similarity(b_ids1, b_mask1, b_ids2, b_mask2, epoch)
        y_hat = logit.sigmoid()
        # scaling output to take care of the sigmoid fxn
        b_labels = b_labels / 5.0
        b_labels = torch.tensor(b_labels, requires_grad=True)
        y_hat = torch.tensor(y_hat, requires_grad=True)

        # Calculate the Mean Squared Error loss
        similarity_loss = sts_weight * F.mse_loss(y_hat, b_labels)
        optimizer.zero_grad()
        similarity_loss.backward()
        optimizer.step()

        train_loss += similarity_loss.item()
        num_batches += 1

    return train_loss / num_batches


def test_model(args):
    with torch.no_grad():
        device = torch.device('cuda') if args.use_gpu else torch.device('cpu')
        saved = torch.load(args.filepath)
        config = saved['model_config']

        model = MultitaskBERT(config)
        model.load_state_dict(saved['model'])
        model = model.to(device)
        print(f"Loaded model to test from {args.filepath}")

        test_model_multitask(args, model, device)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--sst_train", type=str, default="data/ids-sst-train.csv")
    parser.add_argument("--sst_dev", type=str, default="data/ids-sst-dev.csv")
    parser.add_argument("--sst_test", type=str, default="data/ids-sst-test-student.csv")

    parser.add_argument("--para_train", type=str, default="data/quora-train.csv")
    parser.add_argument("--para_dev", type=str, default="data/quora-dev.csv")
    parser.add_argument("--para_test", type=str, default="data/quora-test-student.csv")

    parser.add_argument("--sts_train", type=str, default="data/sts-train.csv")
    parser.add_argument("--sts_dev", type=str, default="data/sts-dev.csv")
    parser.add_argument("--sts_test", type=str, default="data/sts-test-student.csv")

    parser.add_argument("--seed", type=int, default=11711)
    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--option", type=str,
                        help='pretrain: the BERT parameters are frozen; finetune: BERT parameters are updated',
                        choices=('pretrain', 'finetune'), default="pretrain")
    parser.add_argument("--use_gpu", action='store_true')

    parser.add_argument("--sst_dev_out", type=str, default="predictions/sst-dev-output.csv")
    parser.add_argument("--sst_test_out", type=str, default="predictions/sst-test-output.csv")

    parser.add_argument("--para_dev_out", type=str, default="predictions/para-dev-output.csv")
    parser.add_argument("--para_test_out", type=str, default="predictions/para-test-output.csv")

    parser.add_argument("--sts_dev_out", type=str, default="predictions/sts-dev-output.csv")
    parser.add_argument("--sts_test_out", type=str, default="predictions/sts-test-output.csv")

    # hyper parameters
    parser.add_argument("--batch_size", help='sst: 64 can fit a 12GB GPU', type=int, default=64)
    parser.add_argument("--hidden_dropout_prob", type=float, default=0.3)
    parser.add_argument("--lr", type=float, help="learning rate, default lr for 'pretrain': 1e-3, 'finetune': 1e-5",
                        default=1e-3)
    parser.add_argument("--local_files_only", action='store_true')

    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = get_args()
    args.filepath = f'{args.option}-{args.epochs}-{args.lr}-multitask.pt'  # save path
    seed_everything(args.seed)  # fix the seed for reproducibility
    train_multitask(args)
    test_model(args)


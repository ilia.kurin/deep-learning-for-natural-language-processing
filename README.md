#  Deep Learning for Natural Language Processing

## Intro
This project involves implementing key components of the BERT model, including multi-head self-attention and a Transformer layer. It focuses on fine-tuning BERT to produce robust sentence embeddings for diverse tasks. This project aims to provide a concise and understandable implementation of the BERT Model, based on the transformer architecture and its applications in different tasks such as:
- Paraphrase detection 
- Semantic Textual Similarity
- Sentiment Analysis

## Prerequisites 
1. **Hardware Requirement**: A machine with GPUs for efficient model training.
2. **Software Requirement**: Ensure Anaconda or Miniconda is installed.
3. **Dependencies**: Navigate to project root directory and run `./setup.sh`.
4. **Environment Activation**: Activate the conda environment using: `conda activate dnlp`. This is required to run project ever time required. 

## Libraries
certifi==2023.5.7
charset-normalizer==3.2.0
colorama==0.4.6
filelock==3.12.2
fsspec==2023.6.0
huggingface-hub==0.16.4
idna==3.4
importlib-metadata==6.8.0
Jinja2==3.1.2
joblib==1.3.1
MarkupSafe==2.1.3
mpmath==1.3.0
networkx==3.1
numpy==1.24.4
packaging==23.1
pandas==2.0.3
Pillow==9.3.0
pyarrow==13.0.0
python-dateutil==2.8.2
pytz==2023.3
PyYAML==6.0.1
regex==2023.8.8
requests==2.31.0
safetensors==0.3.3
scikit-learn==1.3.0
scipy==1.10.1
six==1.16.0
sklearn==0.0.post5
sympy==1.12
threadpoolctl==3.2.0
tokenizers==0.13.3
torch==2.0.1
torchaudio==2.0.2+cu117
torchvision==0.15.2+cu117
tqdm==4.65.0
transformers==4.32.1
typing_extensions==4.7.1
tzdata==2023.3
urllib3==2.0.3
zipp==3.16.2


## Quick Run
1. Make sure Conda environement is up and running. `conda activate dnlp`
2. For a multitask model, run `python multitask_classifier.py --use_gpu --option finetune --epochs 10 --lr 1e-5 --batch_size=64 --hidden_dropout_prob=0.5`.

## Methodology

**Shared weights:** We have decided to use shared weights architecture to incorporate knowledge from all the tasks. But due to dataset imbalance it might be more wise to have separate bert models for each of the tasks.

**Loss normalisation:** Paraphrase dataset is significantly bigger than others. Due to this huge dataset imbalance it was decided to use loss normalization. All loss functions are weighted by the dataset size to help minimize the impact of paraphrase dataset significantly decreasing perfromance in other tasks. Weight normalization did not significantly help with performance as the problem is much bigger.


**Joint training:** To improve multitask training performance, joint training was used. All the tasks are trained simultaneously taking into account different dataset sizes. To test the affect of different dataset sizes we tried two options, zip (using equal number of dataset items) and zip_longest (using all the dataset items taking into account Nones). Using zip, performance of paraphrase has decreased, but of other tasks performance has improved. With zip_longest problem stayed as training joingly happens only during few first epochs. Later paraphrase is trained alone. So, overall joint training did not yield any performance improvements in the current model state.

## Experiments

**Ilia Kurin:**

**PCGrad:** To help with gradient directions gradient surgery for multi-task was used. Due to the error in implementation, memory was leaking causing memory overflow after few epochs. Which made us remove it in final model training

**Deepening the model:** Adding linear layers on top of embedding or specific task layers did not improve performance

**Pluzhnikov Ivan:**

**1. Additional BiLSTM layer after BERT in forward (In the code: # Ivan, update 1).**

Goal: Using an additional layer to catch higher-level features.

Performance:
Paraphrase detection accuracy: 0.625

Sentiment classification accuracy: 0.517

Semantic Textual Similarity correlation: 0.129

dev sentiment acc :: 0.517

dev paraphrase acc :: 0.625

dev sts corr :: 0.129

Compared with the same model without BiLSTM:
Paraphrase detection accuracy: 0.625

Sentiment classification accuracy: 0.526

Semantic Textual Similarity correlation: 0.220

dev sentiment acc :: 0.526

dev paraphrase acc :: 0.625

dev sts corr :: 0.220

Conclusion: The result became worse, the improvement was not included in the final model.


**2. Additional Dataset for pretraining - https://huggingface.co/datasets/paws . (In the code: # Ivan, update 2).**

Goal: Add dataset structually similar with the dataset for the paraphrase. Expect model to better work with the additional data for training.  

Performance:
Epoch 0: train loss :: 2.461

Epoch 1: train loss :: 2.460

Epoch 2: train loss :: 2.453

Epoch 3: train loss :: 2.458

Conclusion: As part of the experiment, the Loss practically does not decrease. The improvement was not included in the final model.


**3. Additional Dataset for pretraining - https://huggingface.co/datasets/app_reviews . (In the code: # Ivan, update 3).**

Goal: Add dataset structually similar with the dataset for the sst. Expect model to better work with the additional data for training.  

Performance:
Epoch 0: train loss :: 3.068

Conclusion: As part of the experiment, the start Loss is too high for as a result of additional preteaining. Later results werent much better. The improvement was not included in the final model.


**4. Additional Dataset for pretraining - https://huggingface.co/datasets/sem_eval_2014_task_1 . (In the code: # Ivan, update 4).**

Goal: Add dataset structually similar with the dataset for the sts. Expect model to better work with the additional data for training.  

Performance:
Epoch 0: train loss :: 2.484

Epoch 1: train loss :: 2.461

Epoch 2: train loss :: 2.450

Epoch 3: train loss :: 2.444

Conclusion: As part of the experiment, the Loss practically does not decrease. The improvement was not included in the final model.



**5. Additional Dataset for pretraining - https://huggingface.co/datasets/bookcorpus . (In the code: # Ivan, update 5).**

Goal: Make model better understand English language in general. Tryed to predict the last word in the sentence.

Performance:
Epoch 0: train loss :: 2.472

Epoch 1: train loss :: 2.459

Epoch 2: train loss :: 2.461

Epoch 3: train loss :: 2.460

Conclusion: As part of the experiment, the Loss practically does not decrease. The improvement was not included in the final model.


**6. Additional features for embendings. (In the code: # Ivan, update 6).**

Goal: Add udditional features based on input_ids in forward can help model to work better.
- SRL embendings as additional feature - too slow. Decreased speed of working in approx. 10 times.
- Amount of the special tokens, Amount of apper tokens, Amount of numeratic tokens.

Performance:
Paraphrase detection accuracy: 0.625

Sentiment classification accuracy: 0.510

Semantic Textual Similarity correlation: 0.178

dev sentiment acc :: 0.510

dev paraphrase acc :: 0.625

dev sts corr :: 0.178

Compared with the same model without Additional features for embendings:
Paraphrase detection accuracy: 0.625

Sentiment classification accuracy: 0.526

Semantic Textual Similarity correlation: 0.220

dev sentiment acc :: 0.526

dev paraphrase acc :: 0.625

dev sts corr :: 0.220

Conclusion: The result became worse, the improvement was not included in the final model.


**7. Bert unfreezing. (In the code: # Ivan, update 7).**

Goal: improve the convergence of your model and use transfer learning more efficiently.

Performance:
Paraphrase detection accuracy: 0.631

Sentiment classification accuracy: 0.892

Semantic Textual Similarity correlation: 0.156

dev sentiment acc :: 0.511

dev paraphrase acc :: 0.624

dev sts corr :: 0.162

Compared with the same model without Bert unfreezing:
Paraphrase detection accuracy: 0.625

Sentiment classification accuracy: 0.526

Semantic Textual Similarity correlation: 0.220

dev sentiment acc :: 0.526

dev paraphrase acc :: 0.625

dev sts corr :: 0.220

Conclusion: The result became worse, the improvement was not included in the final model.


**Mawuko Tettey:**
Mawuko Tettey
Adding Bi-LSTM layers for fine-tuning of STS task(accuracy barely improved and training time increased)
Depending the linear layers for STS task but the outcome as similar to the previous 
Reducing batch size also reduced the training loss
L1 loss function to prevent overfitting but was replaced with a regular MSE loss function as the model was not overfitting


## Results
Following are the results obtained by generalised fine tune model on given NLP tasks.

| Task                          | Train Accuracy  | Validation Accuracy |
| ----------------------------- |---------------- | -------------- |
| Paraphrase detection          |     63.0%         |      62.5%       |
| Semantic Textual Similarity   |     97.8%         |      52.6%       |
| Sentiment Analysis            |     0.289         |      0.220       |

The model overfits on STS task and does not really train for Paraphrase task.

## Contribution

#### Group 02: Code Transformers
- Ilia Kurin: Wrote AdamW optimizer forward method. Responsible for paraphrase detection part in multitask. Did joint multitask training, weight normalization. Did unsuccessful performance improvement tests such as pcgrad and deepening the model. Responsible for SLURM scripts and running the model on cluster. 

- Mawuko Tettey: Focus: Semantic Textual Similarity
Developing the additional layers for fine-tuning the BERT model using two additional linear layers ontop of the BERT model and a cosine-similarity 
to predict the similarity. An MSE-loss function was used for this task. 
Running code on cluster for each update.

- Karan Sharma: Implemented classifier.py and completed the model pipeline for sentiment analysis. Carried out failed optimization like deeping fully connected layer before classifier, dropout, lr scheduler. Did manual hyper parameter tuning. 

- Pranay Bhatia: Responsible to create documentation and AI card. Triend manual hyper parameter tuning and integrating Optuna for automated hyperparameter tuning. 

- Pluzhnikov Ivan: 
    Responsible for the forward part.

    Responsible for the train_multitask part.

    Responcible for the implementation of the Bert.py

    Responsible for the 'Additional BiLSTM layer after BERT in forward' experiment.

    Responsible for the 'Additional Dataset for pretraining - https://huggingface.co/datasets/paws' experiment.

    Responsible for the 'Additional Dataset for pretraining - https://huggingface.co/datasets/app_reviews' experiment.

    Responsible for the 'Additional Dataset for pretraining - https://huggingface.co/datasets/sem_eval_2014_task_1' experiment.

    Responsible for the 'Additional Dataset for pretraining - https://huggingface.co/datasets/bookcorpus' experiment.

    Responsible for the 'Additional features for embending' experiment.
    
    Responsible for the 'Bert unfreezing' experiment.


## License
This project is licensed under the APACHE License. Please refer to the `LICENSE` file for more details.
